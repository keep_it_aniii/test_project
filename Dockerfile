FROM rocker/tidyverse
COPY . /project
WORKDIR /project
RUN R -e 'install.packages("renv")'
RUN R -e "renv::restore()"
EXPOSE 8000
CMD ["Rscript", "API/src/run-api.R"]