const express = require('express');
const app = express();

app.get("/", (req,res) => {
    res.send("<h1>Test dockerisation for nodejs application</h1>");
});

app.listen(8000,() => {
    console.log("App running on port 8000...");
});